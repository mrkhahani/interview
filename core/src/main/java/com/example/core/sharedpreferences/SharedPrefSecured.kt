package com.example.core.sharedpreferences

import android.content.Context
import android.content.SharedPreferences
import com.example.core.encryption.Base64
import com.example.core.encryption.Encryption
import com.example.core.encryption.IShouldStore

class SharedPrefSecured(
    private var context:Context,
    private var password: String
) : ISharedPref {

    private val sharedPreferences: SharedPreferences =
        context.getSharedPreferences("app", Context.MODE_PRIVATE)

    override fun write(key: String, value: String) {

        putShouldStore(Base64.encodeString(key), encryptValue(value))
            .commit()

    }

    private fun encryptValue(value: String): IShouldStore = Encryption.encrypt(password, value)

    private fun putShouldStore(key: String, info: IShouldStore): SharedPreferences.Editor {

        return with(sharedPreferences.edit()) {
            putString("$key:t", info.encryptedText)
            putString("$key:i", info.iv)
            putString("$key:s", info.salt)
        }
    }

    @Throws(IllegalStateException::class)
    override fun read(key: String): String = Encryption.decrypt(
        password,
        Encryption.ShouldStore(
            getShouldStore(key).salt,
            getShouldStore(key).iv,
            getShouldStore(key).encryptedText
        )
    )

    /**
     * Clear all sharedpreference data for secure and simple preferences
     */
    override fun clear() {
        SharedPrefCleaner.clear(context)
    }

    @Throws(IllegalStateException::class)
    private fun getShouldStore(key: String): IShouldStore {
        val keyEncoded = Base64.encodeString(key)
        val encryptedText = sharedPreferences.getString("$keyEncoded:t", null)
        val iv = sharedPreferences.getString("$keyEncoded:i", null)
        val salt = sharedPreferences.getString("$keyEncoded:s", null)

        if (salt == null || iv == null || encryptedText == null) {
            throw IllegalStateException("Stored info is null !!")
        }

        return Encryption.ShouldStore(salt, iv, encryptedText)
    }

}