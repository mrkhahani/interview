package com.example.core.sharedpreferences

import android.content.Context
import android.content.SharedPreferences
import com.example.core.encryption.Base64
import java.lang.IllegalStateException

class SharedPrefSimple(
    private var context: Context
) : ISharedPref {

    private val sharedPreferences: SharedPreferences =
        context.getSharedPreferences("app", Context.MODE_PRIVATE)

    override fun write(key: String, value: String) {

        with(sharedPreferences.edit()) {
            putString(Base64.encodeString(key), Base64.encodeString(value))
                .commit()
        }
    }

    @Throws(IllegalStateException::class)
    override fun read(key: String): String {
        val encodedValue = sharedPreferences.getString(Base64.encodeString(key), null)
            ?: throw IllegalStateException("Value is null !!")

        return Base64.decodeString(encodedValue)
    }

    /**
     * Clear all sharedpreference data for secure and simple preferences
     */
    override fun clear() {
        SharedPrefCleaner.clear(context)
    }

}