package com.example.core.sharedpreferences

import android.annotation.SuppressLint
import android.content.Context
import android.content.SharedPreferences

internal class SharedPrefCleaner {
    companion object{
        @SuppressLint("ApplySharedPref")
        fun clear(context:Context){
                context.getSharedPreferences("app", Context.MODE_PRIVATE)
                    .edit()
                    .clear()
                    .commit()
        }
    }
}