package com.example.core.sharedpreferences


interface ISharedPref {

    fun write(key: String, value: String)

    fun read(key: String): String

    fun clear()
}