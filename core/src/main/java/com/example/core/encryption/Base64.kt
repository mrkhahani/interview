package com.example.core.encryption

import android.util.Base64

class Base64 {
    companion object: IBase64 {
        override fun encodeString(text: String): String =
            Base64.encodeToString(text.toByteArray(Charsets.UTF_8), Base64.NO_WRAP)


        override fun decodeString(encoded: String): String =
            Base64.decode(encoded, Base64.NO_WRAP).toString(Charsets.UTF_8)

    }
}