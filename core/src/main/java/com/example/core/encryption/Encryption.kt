package com.example.core.encryption

import android.util.Base64
import java.security.AlgorithmParameters
import java.security.SecureRandom
import java.security.spec.KeySpec
import javax.crypto.Cipher
import javax.crypto.SecretKey
import javax.crypto.SecretKeyFactory
import javax.crypto.spec.IvParameterSpec
import javax.crypto.spec.PBEKeySpec
import javax.crypto.spec.SecretKeySpec


class Encryption(){


    class ShouldStore(
        override val salt: String,
        override val iv: String,
        override val encryptedText: String
    ): IShouldStore

    companion object : IEncryption {

        /**
         * @param password stored data in server and is unique
         * @param text information that want to keep secret and encrypted
         *
         * @return Information required for retrieve encrypted back. store them on place you want.
         *
         * @author Mohammadreza Khahani
         */
        override fun encrypt(password: String, text: String): IShouldStore {

            //generate keys
            val factory: SecretKeyFactory = SecretKeyFactory.getInstance(algorithm)
            val salt = generateSalt()
            val spec: KeySpec = PBEKeySpec(password.toCharArray(), salt, iterationCount, 256)
            val tmp: SecretKey = factory.generateSecret(spec)
            val secret: SecretKey = SecretKeySpec(tmp.getEncoded(), "AES")

            // make it cipher
            val cipher: Cipher = Cipher.getInstance(transformation)
            cipher.init(Cipher.ENCRYPT_MODE, secret)
            val params: AlgorithmParameters = cipher.getParameters()
            val iv = params.getParameterSpec(IvParameterSpec::class.java).getIV()
            val ciphertext: ByteArray =
                cipher.doFinal(text.toByteArray(Charsets.UTF_8))
            val storeSalt = Base64.encodeToString(salt, Base64.NO_WRAP)
            return ShouldStore(
                storeSalt,
                Base64.encodeToString(iv, Base64.NO_WRAP),
                Base64.encodeToString(ciphertext, Base64.NO_WRAP)
            )
        }

        override fun decrypt(password: String, storedInfo: IShouldStore): String {

            val salt = Base64.decode(storedInfo.salt, Base64.DEFAULT)
            val iv = Base64.decode(storedInfo.iv, Base64.DEFAULT)
            val encrypted = Base64.decode(storedInfo.encryptedText, Base64.DEFAULT)

            val factory: SecretKeyFactory = SecretKeyFactory.getInstance(algorithm)
            val spec: KeySpec = PBEKeySpec(
                password.toCharArray(),
                salt,
                iterationCount,
                256
            )
            val tmp: SecretKey = factory.generateSecret(spec)
            val secret: SecretKey = SecretKeySpec(tmp.getEncoded(), "AES")

            val cipher = Cipher.getInstance(transformation)
            cipher.init(
                Cipher.DECRYPT_MODE,
                secret,
                IvParameterSpec(iv)
            )
            val plaintext =
                String(
                    cipher.doFinal(encrypted),
                    Charsets.UTF_8
                )
            return plaintext
        }

        private val transformation = "AES/CBC/PKCS5Padding"
        private val algorithm = "PBKDF2withHmacSHA1" //PBKDF2WithHmacSHA256
        private val iterationCount = 10 //65536


        private fun generateSalt(): ByteArray {
            val random = SecureRandom()
            val bytes = ByteArray(8)
            random.nextBytes(bytes)
            return bytes
        }

    }

}