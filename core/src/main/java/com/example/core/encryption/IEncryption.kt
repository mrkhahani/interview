package com.example.core.encryption


interface IEncryption {
    fun encrypt(password: String, text: String): IShouldStore
    fun decrypt(password: String, storedInfo: IShouldStore): String
}