package com.example.core.encryption

interface IShouldStore {
    val salt: String
    val iv: String
    val encryptedText: String
}