package com.example.core.encryption

interface IBase64 {
    fun encodeString(text: String): String
    fun decodeString(encoded: String): String
}