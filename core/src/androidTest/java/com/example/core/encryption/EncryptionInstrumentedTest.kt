package com.example.core.encryption

import androidx.test.filters.SmallTest
import androidx.test.runner.AndroidJUnit4
import org.junit.Assert.assertEquals
import org.junit.Test
import org.junit.runner.RunWith

/**
 * Instrumented test, which will execute on an Android device.
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
@RunWith(AndroidJUnit4::class)
@SmallTest
class EncryptionInstrumentedTest {

    @Test
    fun encryption_is_correct() {
        val text = "khahani"
        val password = "Iamkhahani"
        val encrypted = Encryption.encrypt(password,text)
        val result = Encryption.decrypt(password, encrypted)
        assertEquals(text, result)
    }

    @Test
    fun base64_is_correct(){
        val text = "khahani"
        val encoded = Base64.encodeString(text)
        val result = Base64.decodeString(encoded)
        assertEquals(text, result)
    }

}
