package com.tasvirara.core.sharedpreference

import android.content.Context
import androidx.test.filters.SmallTest
import androidx.test.platform.app.InstrumentationRegistry
import androidx.test.runner.AndroidJUnit4
import com.example.core.encryption.Base64
import com.example.core.encryption.Encryption
import com.example.core.sharedpreferences.SharedPrefSecured
import com.example.core.sharedpreferences.SharedPrefSimple
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.Assert.*
import org.junit.Before

/**
 * Instrumented test, which will execute on an Android device.
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
@RunWith(AndroidJUnit4::class)
@SmallTest
class SharedPreferenceInstrumentedTest {

    lateinit var instrumentationContext: Context

    @Before
    fun setup() {
        instrumentationContext = InstrumentationRegistry.getInstrumentation().context
    }


    @Test
    fun shared_prefence_secure_is_correct(){
        val text = "khahani"
        val password = "Iamkhahani"
        val mobile = "09052867813"

        val sharedPre = SharedPrefSecured(instrumentationContext, password)
        sharedPre.write("mobile", mobile)
        val result = sharedPre.read("mobile")

        assertEquals(mobile, result)

    }

    @Test
    fun shared_preference_simple_is_correct(){
        val key = "mobile"
        val value = "09052867813"

        val sharedPref = SharedPrefSimple(instrumentationContext)
        sharedPref.write(key, value)
        val result = sharedPref.read(key)

        assertEquals(value, result)
    }
}
