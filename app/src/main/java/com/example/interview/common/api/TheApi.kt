package com.example.interview.common.api

import com.google.gson.GsonBuilder
import hu.akarnokd.rxjava3.retrofit.RxJava3CallAdapterFactory
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Response
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.io.IOException


object TheApi {

    private val baseUrl = "https://api.foursquare.com/v2/"

    inline fun <reified T> ready(): T {
        return getRetrofit()
            .create(T::class.java)
    }

    fun getRetrofit(): Retrofit {
        return Retrofit.Builder()
            .baseUrl(baseUrl)
            .client(getClient())
            .addCallAdapterFactory(RxJava3CallAdapterFactory.create())
            .addConverterFactory(
                getGsonConverterFactory()
            )
            .build()
    }

    private fun getClient(): OkHttpClient {
        return OkHttpClient.Builder()
            .addInterceptor(getRequestInterceptor())
            .addInterceptor(getLoggingInterceptor())
            .build()
    }

    private fun getRequestInterceptor(): Interceptor {
        return object : Interceptor {
            @Throws(IOException::class)
            override
            fun intercept(chain: Interceptor.Chain): Response {

                return chain.proceed(
                    chain.request()
                        .newBuilder()
                        .url(
                            chain.request().url.newBuilder()
                                .addQueryParameter(
                                    "client_id",
                                    "ZDRIFDX0L4PDE331WSJJY2AWPZCQPAELEEMIWHVZWIOWYCR3"
                                )
                                .addQueryParameter(
                                    "client_secret",
                                    "MOJJGUI4UYVQREHTEGHBJ0O04BSBAXCUFYIAVMDT224PI2CA"
                                )
                                .addQueryParameter(
                                    "v",
                                    "20200427"
                                )
                                .build()
                        ).build()
                )
            }
        }
    }

    private fun getGsonConverterFactory(): GsonConverterFactory {
        return GsonConverterFactory.create(
            GsonBuilder()
                .excludeFieldsWithoutExposeAnnotation()
                .create()
        )
    }

    private fun getLoggingInterceptor(): HttpLoggingInterceptor {
        val logging = HttpLoggingInterceptor()
        logging.apply { logging.level = HttpLoggingInterceptor.Level.BODY }
        return logging
    }
}