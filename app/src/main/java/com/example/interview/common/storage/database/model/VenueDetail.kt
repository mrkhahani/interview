package com.example.interview.common.storage.database.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.Index
import androidx.room.PrimaryKey

@Entity(tableName = "venue_detail", indices = [Index(value = ["server_id"], unique = true)])
data class VenueDetail(
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    val id: Int,
    @ColumnInfo(name = "server_id")
    val serverId: String,
    @ColumnInfo(name = "likes")
    val likes: Int,
    @ColumnInfo(name = "rate")
    val rate: Double = 0.0,
    @ColumnInfo(name = "ratingSignal")
    val ratingSingal: Int = 0,
    @ColumnInfo(name = "phone")
    val phone: String = ""
)