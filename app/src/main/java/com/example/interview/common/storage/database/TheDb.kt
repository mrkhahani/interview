package com.example.interview.common.storage.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.sqlite.db.SupportSQLiteDatabase
import com.example.interview.common.storage.database.model.Venue
import com.example.interview.common.storage.database.model.VenueDetail
import com.example.interview.venuesNearUser.databaseImpl.venue.VenueDoa
import com.example.interview.venuesNearUser.databaseImpl.venueDetail.VenueDetailDoa

@Database(
    entities = [Venue::class,
        VenueDetail::class]
    , version = 4, exportSchema = false
)
abstract class TheDb : RoomDatabase() {

    abstract fun venueDoa(): VenueDoa
    abstract fun venueDetailDoa(): VenueDetailDoa

    companion object {
        @Volatile
        private var INSTANCE: TheDb? = null

        fun getInstance(context: Context): TheDb {
            synchronized(this) {
                var instance = INSTANCE
                if (instance == null) {
                    instance = Room.databaseBuilder(
                        context.applicationContext,
                        TheDb::class.java,
                        "loc_db"
                    ).addCallback(object : RoomDatabase.Callback() {
                        override fun onCreate(db: SupportSQLiteDatabase) {
                            super.onCreate(db)
                        }

                        override fun onOpen(db: SupportSQLiteDatabase) {
                            super.onOpen(db)
                        }
                    }).fallbackToDestructiveMigration()
                        .build()
                    INSTANCE = instance
                }
                return instance
            }
        }

//        val MIGRATION_ADD_VENUE_DETAILS = object : Migration(1, 2) {
//            override fun migrate(database: SupportSQLiteDatabase) {
//                database.execSQL("CREATE TABLE `venue_detail` ( " +
//                        " `id` INTEGER PRIMARY KEY AUTOINCREMENT, " +
//                        " `server_id` TEXT NOT NULL, " +
//                        " `likes` INTEGER NOT NULL DEFAULT 0, "+
//                        " `rate` REAL NOT NULL DEFAULT 0, "+
//                        " `ratingSignal` INTEGER NOT NULL DEFAULT 0, "+
//                        " `phone` TEXT, "+
//                        " PRIMARY KEY(`id`))")
//                database.execSQL("CREATE UNIQUE INDEX idx_server_id ON venue_detail(server_id)")
//            }
//        }
    }
}