package com.example.interview.common.monitor

import android.util.Log
import com.example.interview.BuildConfig

object Logger {
    fun d(tag: String, message: String) {
        if (BuildConfig.DEBUG)
            Log.d(tag, message)
    }
}