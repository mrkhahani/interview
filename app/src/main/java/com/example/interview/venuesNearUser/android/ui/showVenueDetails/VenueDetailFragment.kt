package com.example.interview.venuesNearUser.android.ui.showVenueDetails

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.navArgs
import com.example.interview.R
import com.example.interview.databinding.VenuesNearUserUsecaseShowVenuesDetailsLayoutVenueDetailFragmentBinding
import com.example.interview.venuesNearUser.android.ui.ViewModelFactory
import com.example.interview.venuesNearUser.android.ui._base.BaseFragment
import com.example.interview.venuesNearUser.entities.venueDetail.VenueDetail
import com.example.interview.venuesNearUser.usecases.showVenueDetails.boundary.output.VenueDetailOutputBound
import com.example.interview.venuesNearUser.usecases.showVenueDetails.interactor.inputDto.VenueDetailInput

class VenueDetailFragment : BaseFragment(), VenueDetailOutputBound {

    private lateinit var viewModel: VenueDetailViewModel
    private lateinit var binding: VenuesNearUserUsecaseShowVenuesDetailsLayoutVenueDetailFragmentBinding

    private val args: VenueDetailFragmentArgs by navArgs()

    override fun onCreateViewYourCode(savedInstanceState: Bundle?) {

        app.venueDetailTransaction.setVenueDetailOutputBound(this)

        val venueId = args.venueId
        app.venueDetailController.getVenueDetail(venueId)
    }

    override fun getRoot(): View {
        return binding.root
    }

    override fun initialViewModel() {
        viewModel = ViewModelProvider(
            this, ViewModelFactory(app)
        ).get(VenueDetailViewModel::class.java)
    }

    override fun initialBinding(inflater: LayoutInflater, container: ViewGroup?) {
        binding = DataBindingUtil.inflate(
            inflater,
            R.layout.venues_near_user_usecase_show_venues_details_layout_venue_detail_fragment,
            container,
            false
        )
    }

    override fun letBindingKnowViewModel() {
        binding.viewModel = viewModel
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProvider(
            this,
            ViewModelFactory(
                app
            )
        ).get(VenueDetailViewModel::class.java)
    }

    override fun onVenueDetailArrived(venueDetail: VenueDetail) {
        viewModel.venueDetail = VenueDetailInput().apply {
            this.rate = venueDetail.rate
            this.phone = venueDetail.phone
            this.likes = venueDetail.likes
            this.identifier = venueDetail.identifier
            this.ratingSingal = venueDetail.ratingSingal
        }

        //todo: replace with 2 way binding
        activity?.runOnUiThread {
            binding.likesText.setText(viewModel.venueDetail.likes.toString())
            binding.rateText.rating = viewModel.venueDetail.rate.toFloat()
            binding.phoneText.setText(viewModel.venueDetail.phone)
            binding.ratiningSignalText.setText(viewModel.venueDetail.ratingSingal.toString())
        }
    }

}
