package com.example.interview.venuesNearUser.apiImpl.venueDetail.model.getVenueDetail.response

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class Item____ {
    @SerializedName("id")
    @Expose
    var id: String? = null
    @SerializedName("createdAt")
    @Expose
    var createdAt: Int? = null

}