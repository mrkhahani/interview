package com.example.interview.venuesNearUser.usecases.detectUserMoves.boundary.inputs

import com.example.interview.venuesNearUser.usecases.detectUserMoves.interactor.inputDto.LocationInput

interface MovementInputBound {
    fun onLocationChanged(location: LocationInput)
}