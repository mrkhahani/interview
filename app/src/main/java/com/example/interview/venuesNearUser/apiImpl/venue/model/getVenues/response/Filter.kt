package com.example.interview.venuesNearUser.apiImpl.venue.model.getVenues.response

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class Filter {
    @SerializedName("name")
    @Expose
    var name: String? = null
    @SerializedName("key")
    @Expose
    var key: String? = null

}