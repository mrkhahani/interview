package com.example.interview.venuesNearUser.android.controller

import android.location.Location
import com.example.interview.venuesNearUser.usecases.detectUserMoves.boundary.inputs.MovementInputBound
import com.example.interview.venuesNearUser.usecases.detectUserMoves.interactor.inputDto.LocationInput

class MovementController(var movementInputBound: MovementInputBound) {

    fun locationChanged(location: Location){
        movementInputBound.onLocationChanged(
            LocationInput(
                location.latitude,
                location.longitude
            )
        )
    }

}