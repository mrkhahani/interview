package com.example.interview.venuesNearUser.apiImpl.venueDetail.model.getVenueDetail.response

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class HighlightColor {
    @SerializedName("photoId")
    @Expose
    var photoId: String? = null
    @SerializedName("value")
    @Expose
    var value: Int? = null

}