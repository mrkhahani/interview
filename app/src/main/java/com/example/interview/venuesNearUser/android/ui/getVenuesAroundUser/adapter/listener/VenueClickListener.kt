package com.example.interview.venuesNearUser.android.ui.getVenuesAroundUser.adapter.listener

import com.example.interview.venuesNearUser.usecases.getVenuesAroundUser.interactor.inputDto.VenueInput


interface VenueClickListener {
    fun onItemClicked(venue: VenueInput)
}