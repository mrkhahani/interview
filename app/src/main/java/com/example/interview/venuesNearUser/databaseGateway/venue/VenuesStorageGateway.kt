package com.example.interview.venuesNearUser.databaseGateway.venue

import com.example.interview.venuesNearUser.entities.venue.Venue

interface VenuesStorageGateway {
    fun save(venue: Venue): Long
    fun all(): ArrayList<Venue>
}