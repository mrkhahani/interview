package com.example.interview.venuesNearUser.apiImpl.venueDetail.model.getVenueDetail.response

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class Open {
    @SerializedName("renderedTime")
    @Expose
    var renderedTime: String? = null

}