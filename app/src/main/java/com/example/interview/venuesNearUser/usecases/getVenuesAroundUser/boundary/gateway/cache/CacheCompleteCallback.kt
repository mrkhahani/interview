package com.example.interview.venuesNearUser.usecases.getVenuesAroundUser.boundary.gateway.cache

interface CacheCompleteCallback {
    fun onCached()
    fun onError(e: Throwable)
    fun onNoMoreVenues()
}