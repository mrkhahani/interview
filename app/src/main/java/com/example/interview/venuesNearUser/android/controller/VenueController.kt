package com.example.interview.venuesNearUser.android.controller

import com.example.interview.venuesNearUser.usecases.getVenuesAroundUser.boundary.inputs.VenueInputBound
import com.example.interview.venuesNearUser.usecases.getVenuesAroundUser.interactor.inputDto.VenueInput

class VenueController(var venueInputBound: VenueInputBound) {
    fun loadVeneus(limit: Int, offset: Int): MutableList<VenueInput> {
        return venueInputBound.loadVenues(limit, offset)
    }
}