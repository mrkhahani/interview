package com.example.interview.venuesNearUser.usecases.showVenueDetails.boundary.output

import com.example.interview.venuesNearUser.entities.venueDetail.VenueDetail


interface VenueDetailOutputBound {
    fun onVenueDetailArrived(venueDetail: VenueDetail)
}