package com.example.interview.venuesNearUser.usecases.showVenueDetails.boundary.gateway.cache

interface CacheCompleteCallback {
    fun onCached()
    fun onError(e: Throwable)
}