package com.example.interview.venuesNearUser.apiImpl.venueDetail.model.getVenueDetail.response

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class Attributes {
    @SerializedName("groups")
    @Expose
    var groups: List<Group_____>? = null

}