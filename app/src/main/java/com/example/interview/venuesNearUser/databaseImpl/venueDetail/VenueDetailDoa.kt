package com.example.interview.venuesNearUser.databaseImpl.venueDetail

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.interview.common.storage.database.model.VenueDetail

@Dao
interface VenueDetailDoa {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(venueDetail: VenueDetail): Long

    @Query("SELECT * FROM venue_detail WHERE server_id = :serverId")
    fun get(serverId: String): VenueDetail
}