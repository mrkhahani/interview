package com.example.interview.venuesNearUser.apiImpl.venue.model.getVenues.response

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class SuggestedFilters {
    @SerializedName("header")
    @Expose
    var header: String? = null
    @SerializedName("filters")
    @Expose
    var filters: List<Filter>? =
        null

}