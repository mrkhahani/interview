package com.example.interview.venuesNearUser.usecases.detectUserMoves.boundary.outputs

import com.example.interview.venuesNearUser.usecases.detectUserMoves.interactor.outputDto.LocationOutput


interface MovementOutputBound {
    fun onMoved(location: LocationOutput)
}