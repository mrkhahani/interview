package com.example.interview.venuesNearUser.apiImpl.venue

import com.example.interview.common.api.TheApi
import com.example.interview.venuesNearUser.apiGateway.veneu.VenuesApiGateway
import com.example.interview.venuesNearUser.apiImpl._base.ApiGatewayCommand
import com.example.interview.venuesNearUser.apiImpl.venue.model.getVenues.request.VenuesRequest
import com.example.interview.venuesNearUser.apiImpl.venue.model.getVenues.response.VenuesResponse
import com.example.interview.venuesNearUser.entities.radar.Location
import com.example.interview.venuesNearUser.entities.venue.Venue
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.core.Observer
import io.reactivex.rxjava3.functions.Function

class VenuesApiGatewayImpl :
    ApiGatewayCommand<VenuesResponse, ArrayList<Venue>>(),
    VenuesApiGateway {

    private var lastLocation: Location? = null
    private var offset = 0
    private val LIMIT = 50
    private val RADIUS = 10000

    override fun fetchFirstVenues(location: Location, observerParam: Observer<ArrayList<Venue>>) {
        this.lastLocation = location
        offset = 0
        execute(observerParam)
    }

    override fun fetchRestVenues(observerParam: Observer<ArrayList<Venue>>) {
        if (knowTheLocation()) {
            offset += LIMIT
            execute(observerParam)
        }
    }

    private fun knowTheLocation() = lastLocation != null

    override fun call(): Observable<VenuesResponse> {

        return TheApi.ready<FourSquareService>()
            .getVenues(prepareRequest())
    }

    override fun prepareRequest(): MutableMap<String, String> {
        return VenuesRequest().apply {
            lastLocation = getFormattedLocation()
            this.offset = this@VenuesApiGatewayImpl.offset
            this.limit = LIMIT
            this.radius = RADIUS
        }.fetchMap()
    }

    private fun getFormattedLocation(): String {
        return "${lastLocation?.lat}, ${lastLocation?.lon}"
    }

    override val transform = Function<VenuesResponse, ArrayList<Venue>> { res ->
        val result = ArrayList<Venue>()

        res.response?.groups?.forEach {
            it.items?.forEach {
                result.add(Venue().apply {
                    this.location =
                        Location(
                            it.venue?.location?.lat!!,
                            it.venue?.location?.lng!!
                        )
                    this.address =
                        if (it.venue?.location?.address == null) "" else it.venue?.location?.address!!
                    this.name = it.venue?.name!!
                    this.identifier = it.venue?.id!!
                })
            }
        }

        result
    }
}