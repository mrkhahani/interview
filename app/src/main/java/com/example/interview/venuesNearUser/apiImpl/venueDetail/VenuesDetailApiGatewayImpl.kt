package com.example.interview.venuesNearUser.apiImpl.venueDetail

import com.example.interview.common.api.TheApi
import com.example.interview.venuesNearUser.apiGateway.venueDetail.VenueDetailApiGateway
import com.example.interview.venuesNearUser.apiImpl._base.ApiGatewayCommand
import com.example.interview.venuesNearUser.apiImpl.venueDetail.model.getVenueDetail.request.VenueDetailRequest
import com.example.interview.venuesNearUser.apiImpl.venueDetail.model.getVenueDetail.response.VenueDetailResponse
import com.example.interview.venuesNearUser.entities.venue.Venue
import com.example.interview.venuesNearUser.entities.venueDetail.VenueDetail
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.core.Observer
import io.reactivex.rxjava3.functions.Function

class VenueDetailApiGatewayImpl : ApiGatewayCommand<VenueDetailResponse, VenueDetail>(),
    VenueDetailApiGateway {

    private lateinit var venue: Venue

    override fun fetch(venue: Venue, observerParam: Observer<VenueDetail>) {
        this.venue = venue
        execute(observerParam)
    }

    override fun call(): Observable<VenueDetailResponse> {
        return TheApi.ready<FourSquareService>()
            .getVenueDetail(venue.identifier, prepareRequest())
    }

    override fun prepareRequest(): MutableMap<String, String> {
        return VenueDetailRequest().apply {

        }.fetchMap()
    }

    override val transform = Function<VenueDetailResponse, VenueDetail> { res ->

        val result = VenueDetail()
        result.identifier = try {
            res?.response?.venue?.id!!
        } catch (e: Exception) {
            ""
        }
        result.ratingSingal = try {
            res.response?.venue?.ratingSignals!!
        } catch (e: Exception) {
            0
        }
        result.rate = try {
            res.response?.venue?.rating!!
        } catch (e: Exception) {
            0.0
        }
        result.phone = try {
            res.response?.venue?.contact?.formattedPhone!!
        } catch (e: Exception) {
            ""
        }
        result.likes = try {
            res.response?.venue?.likes?.count!!
        } catch (e: Exception) {
            0
        }


        result
    }
}