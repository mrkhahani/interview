package com.example.interview.venuesNearUser.apiImpl.venue.model.getVenues.request

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class VenuesRequest {

    @Expose
    @SerializedName("ll")
    var lastLocation:String = ""

    @Expose
    @SerializedName("offset")
    var offset:Int = 0

    @Expose
    @SerializedName("radius")
    var radius: Int = 0

    @Expose
    @SerializedName("limit")
    var limit: Int = 0

    fun fetchMap() : MutableMap<String, String>{
        return HashMap<String, String>().apply {
            this["ll"] = lastLocation
            this["offset"] = offset.toString()
            this["radius"] = radius.toString()
            this["limit"] = limit.toString()
        }
    }
}