package com.example.interview.venuesNearUser.entities.venue

import com.example.interview.venuesNearUser.entities.radar.Location

class Venue : Cloneable {
    var identifier: String = ""
    var location: Location =
        Location(0.0, 0.0)
    var name:String = ""
    var address:String = ""

    public override fun clone(): Any {
        return super.clone()
    }
}