package com.example.interview.venuesNearUser.apiImpl.venue

import com.example.interview.venuesNearUser.apiImpl.venue.model.getVenues.response.VenuesResponse
import io.reactivex.rxjava3.core.Observable
import retrofit2.http.GET
import retrofit2.http.QueryMap

interface FourSquareService {

    @GET("venues/explore")
    fun getVenues(@QueryMap request:Map<String, String>):Observable<VenuesResponse>

}