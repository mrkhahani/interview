package com.example.interview.venuesNearUser.apiImpl.venueDetail.model.getVenueDetail.response

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class Stats {
    @SerializedName("tipCount")
    @Expose
    var tipCount: Int? = null

}