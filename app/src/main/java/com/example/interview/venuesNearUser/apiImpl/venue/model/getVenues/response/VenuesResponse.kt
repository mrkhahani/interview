package com.example.interview.venuesNearUser.apiImpl.venue.model.getVenues.response

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class VenuesResponse {
    @SerializedName("meta")
    @Expose
    var meta: Meta? = null
    @SerializedName("response")
    @Expose
    var response: Response? = null

}