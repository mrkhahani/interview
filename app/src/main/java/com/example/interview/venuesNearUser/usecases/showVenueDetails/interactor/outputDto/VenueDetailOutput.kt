package com.example.interview.venuesNearUser.usecases.showVenueDetails.interactor.outputDto

class VenueDetailOutput {
    var identifier: String = ""
    var likes: Int = 0
    var rate: Double = 0.0
    var ratingSingal: Int = 0
    var phone: String = ""
}