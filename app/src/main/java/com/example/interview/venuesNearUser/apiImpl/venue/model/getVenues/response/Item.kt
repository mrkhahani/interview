package com.example.interview.venuesNearUser.apiImpl.venue.model.getVenues.response

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class Item {
    @SerializedName("reasons")
    @Expose
    var reasons: Reasons? = null
    @SerializedName("venue")
    @Expose
    var venue: Venue? =
        null
    @SerializedName("referralId")
    @Expose
    var referralId: String? = null

}