package com.example.interview.venuesNearUser.apiImpl.venue.model.getVenues.response

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class ReasonItem {
    @SerializedName("summary")
    @Expose
    var summary: String? = null
    @SerializedName("type")
    @Expose
    var type: String? = null
    @SerializedName("reasonName")
    @Expose
    var reasonName: String? = null

}