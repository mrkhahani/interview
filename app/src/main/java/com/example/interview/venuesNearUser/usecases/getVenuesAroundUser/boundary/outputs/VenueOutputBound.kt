package com.example.interview.venuesNearUser.usecases.getVenuesAroundUser.boundary.outputs

interface VenueOutputBound {
    fun onNearVenuesChanged()
}