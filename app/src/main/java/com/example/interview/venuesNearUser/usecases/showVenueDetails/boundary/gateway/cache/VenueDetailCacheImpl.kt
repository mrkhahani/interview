package com.example.interview.venuesNearUser.usecases.showVenueDetails.boundary.gateway.cache

import com.example.interview.venuesNearUser.apiGateway.venueDetail.VenueDetailApiGateway
import com.example.interview.venuesNearUser.databaseGateway.venueDetail.VenueDetailStorageGateway
import com.example.interview.venuesNearUser.entities.venue.Venue

class VenueDetailCacheImpl(
    private val venueDetailApi: VenueDetailApiGateway,
    private val venueDetailStorageGateway: VenueDetailStorageGateway,
    private val venue: Venue,
    callback: CacheCompleteCallback
) : VenueDetailCache(callback) {

    override fun fetch() {
        venueDetailApi.fetch(venue, whenVenuesReceived())
    }

    override fun store() {
        venueDetailStorageGateway.save(venuesDetail)
    }
}