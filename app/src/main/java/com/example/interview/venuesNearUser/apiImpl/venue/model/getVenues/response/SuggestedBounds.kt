package com.example.interview.venuesNearUser.apiImpl.venue.model.getVenues.response

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class SuggestedBounds {
    @SerializedName("ne")
    @Expose
    var ne: Ne? = null
    @SerializedName("sw")
    @Expose
    var sw: Sw? =
        null

}