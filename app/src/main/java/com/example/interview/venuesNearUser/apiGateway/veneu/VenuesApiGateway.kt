package com.example.interview.venuesNearUser.apiGateway.veneu

import com.example.interview.venuesNearUser.entities.radar.Location
import com.example.interview.venuesNearUser.entities.venue.Venue
import io.reactivex.rxjava3.core.Observer

interface VenuesApiGateway {
    fun fetchFirstVenues(location: Location, observerParam: Observer<ArrayList<Venue>>)
    fun fetchRestVenues(observerParam: Observer<ArrayList<Venue>>)
}