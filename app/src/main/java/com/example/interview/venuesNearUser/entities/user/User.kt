package com.example.interview.venuesNearUser.entities.user

import com.example.interview.venuesNearUser.entities.radar.Distance
import com.example.interview.venuesNearUser.entities.radar.Location

class User {
    companion object {
        @JvmStatic
        private var latestLocation: Location =
            Location(
                0.0,
                0.0
            )

        @JvmStatic
        private var minimumAcceptedMove = 100
    }

    fun moves(location: Location) {
        if (isAcceptedMove(location)) {
            latestLocation = location
        }
    }

    fun isMoved(location: Location): Boolean {
        val lastLocation = getLatestLocation()
        return location.lat == lastLocation.lat && location.lon == lastLocation.lon
    }

    private fun isAcceptedMove(new: Location): Boolean {
        return Distance.estimate(
            new,
            latestLocation
        ) >= minimumAcceptedMove
    }

    fun getLatestLocation(): Location =
        latestLocation
}