package com.example.interview.venuesNearUser.apiImpl.venueDetail

import com.example.interview.venuesNearUser.apiImpl.venueDetail.model.getVenueDetail.response.VenueDetailResponse
import io.reactivex.rxjava3.core.Observable
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.QueryMap

interface FourSquareService {
    @GET("venues/{venueId}")
    fun getVenueDetail(@Path("venueId") venueId: String, @QueryMap request: Map<String, String>): Observable<VenueDetailResponse>
}