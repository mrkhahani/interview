package com.example.interview.venuesNearUser.apiGateway.venueDetail

import com.example.interview.venuesNearUser.entities.venue.Venue
import com.example.interview.venuesNearUser.entities.venueDetail.VenueDetail
import io.reactivex.rxjava3.core.Observer

interface VenueDetailApiGateway {
    fun fetch(venue: Venue, observerParam: Observer<VenueDetail>)
}