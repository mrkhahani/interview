package com.example.interview.venuesNearUser.usecases.detectUserMoves.interactor.outputDto

class LocationOutput {
    var lat:Double = 0.0
    var lon:Double = 0.0
}
