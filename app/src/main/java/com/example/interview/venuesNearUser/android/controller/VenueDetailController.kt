package com.example.interview.venuesNearUser.android.controller

import com.example.interview.venuesNearUser.usecases.showVenueDetails.boundary.input.VenueDetailInputBound
import com.example.interview.venuesNearUser.usecases.showVenueDetails.interactor.inputDto.VenueInput

class VenueDetailController(val venueDetailInputBound: VenueDetailInputBound) {

    fun getVenueDetail(venueId: String) {
        venueDetailInputBound.getVenueDetail(VenueInput().apply {
            identifier = venueId
        })
    }
}