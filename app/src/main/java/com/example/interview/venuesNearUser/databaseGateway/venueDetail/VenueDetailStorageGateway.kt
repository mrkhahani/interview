package com.example.interview.venuesNearUser.databaseGateway.venueDetail

import com.example.interview.venuesNearUser.entities.venueDetail.VenueDetail

interface VenueDetailStorageGateway {
    fun save(venueDetail: VenueDetail): Long
    fun get(venueId: String): VenueDetail
}