package com.example.interview.venuesNearUser.usecases.showVenueDetails.boundary.input

import com.example.interview.venuesNearUser.usecases.showVenueDetails.interactor.inputDto.VenueInput

interface VenueDetailInputBound {
    fun getVenueDetail(venue: VenueInput)
}