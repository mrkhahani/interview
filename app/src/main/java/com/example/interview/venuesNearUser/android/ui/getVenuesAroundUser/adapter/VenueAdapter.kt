package com.example.interview.venuesNearUser.android.ui.getVenuesAroundUser.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.interview.databinding.VenuesNearUserUsecaseGetVenuesAroundUserLayoutItemVenueListBinding
import com.example.interview.venuesNearUser.android.ui.getVenuesAroundUser.adapter.listener.VenueClickListener
import com.example.interview.venuesNearUser.usecases.getVenuesAroundUser.interactor.inputDto.VenueInput

class VenueAdapter(val clickListener: VenueClickListener) :
    PagedListAdapter<VenueInput, RecyclerView.ViewHolder>(DIFF_CALLBACK) {

    private val VENUE_VIEW_TYPE = 0

    override fun getItemViewType(position: Int): Int {
        return VENUE_VIEW_TYPE
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        if (viewType == VENUE_VIEW_TYPE)
            return VenueViewHolder.from(parent)

        throw IllegalArgumentException("Unknown view type")
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (getItemViewType(position) == VENUE_VIEW_TYPE) {
            if (holder is VenueViewHolder) {
                val item = getItem(position)
                holder.bind(item, clickListener)
            }
        }
    }

    class VenueViewHolder private constructor(private val binding: VenuesNearUserUsecaseGetVenuesAroundUserLayoutItemVenueListBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(
            v: VenueInput?,
            clickListener: VenueClickListener
        ) {
            if (v == null) {
                //todo: use place holder!!!
            } else {
                binding.venue = v
                binding.listener = clickListener
            }
        }

        companion object {
            fun from(parent: ViewGroup): VenueViewHolder {

                val layoutInflater = LayoutInflater.from(parent.context)
                val binding =
                    VenuesNearUserUsecaseGetVenuesAroundUserLayoutItemVenueListBinding.inflate(
                        layoutInflater,
                        parent,
                        false
                    )
                return VenueViewHolder(binding)
            }
        }

    }

    companion object {
        private val DIFF_CALLBACK = object : DiffUtil.ItemCallback<VenueInput>() {
            override fun areItemsTheSame(oldItem: VenueInput, newItem: VenueInput): Boolean {
                return oldItem.identifier == newItem.identifier
            }

            override fun areContentsTheSame(oldItem: VenueInput, newItem: VenueInput): Boolean {
                return oldItem.identifier == newItem.identifier
            }
        }
    }
}