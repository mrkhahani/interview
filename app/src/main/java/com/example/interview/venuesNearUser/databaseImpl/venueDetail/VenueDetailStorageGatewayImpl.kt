package com.example.interview.venuesNearUser.databaseImpl.venueDetail

import com.example.interview.common.storage.database.TheDb
import com.example.interview.venuesNearUser.databaseGateway.venueDetail.VenueDetailStorageGateway
import com.example.interview.venuesNearUser.entities.venueDetail.VenueDetail

class VenueDetailStorageGatewayImpl(private val db: TheDb) :
    VenueDetailStorageGateway {
    private val DEFAULT_ID = 0

    override fun save(venueDetail: VenueDetail): Long {
        val venueDetailDbModel = com.example.interview.common.storage.database.model.VenueDetail(
            DEFAULT_ID,
            venueDetail.identifier,
            venueDetail.likes,
            venueDetail.rate,
            venueDetail.ratingSingal,
            venueDetail.phone
        )
        return db.venueDetailDoa().insert(venueDetailDbModel)
    }

    override fun get(venueId: String): VenueDetail {
        val venueDetailDb = db.venueDetailDoa().get(venueId)
        return VenueDetail().apply {
            identifier = venueDetailDb.serverId
            likes = venueDetailDb.likes
            phone = venueDetailDb.phone
            rate = venueDetailDb.rate
            ratingSingal = venueDetailDb.ratingSingal
        }
    }
}