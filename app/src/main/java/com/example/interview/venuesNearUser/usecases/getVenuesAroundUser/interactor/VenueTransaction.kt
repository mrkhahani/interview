package com.example.interview.venuesNearUser.usecases.getVenuesAroundUser.interactor

import com.example.interview.venuesNearUser.apiGateway.veneu.VenuesApiGateway
import com.example.interview.venuesNearUser.databaseGateway.venue.VenuesStorageGateway
import com.example.interview.venuesNearUser.entities.radar.Location
import com.example.interview.venuesNearUser.entities.radar.Radar
import com.example.interview.venuesNearUser.entities.venue.Venue
import com.example.interview.venuesNearUser.usecases.getVenuesAroundUser.boundary.gateway.cache.CacheCompleteCallback
import com.example.interview.venuesNearUser.usecases.getVenuesAroundUser.boundary.gateway.cache.VenueCacheOnUserMoved
import com.example.interview.venuesNearUser.usecases.getVenuesAroundUser.boundary.inputs.VenueInputBound
import com.example.interview.venuesNearUser.usecases.getVenuesAroundUser.boundary.outputs.VenueOutputBound
import com.example.interview.venuesNearUser.usecases.getVenuesAroundUser.interactor.inputDto.VenueInput

class VenueTransaction(
    private var venuesStorageGateway: VenuesStorageGateway,
    private var venuesApiGateway: VenuesApiGateway
) : VenueInputBound {

    @Volatile
    private var nearVenues: ArrayList<Venue> = ArrayList()

    private var venueOutputBound: VenueOutputBound? = null

    private var stopCallingApi = false

    private val cacheCompleted = object : CacheCompleteCallback {
        override fun onCached() {
            findNearVenues()
            requiredVenueOutputBound()
            venueOutputBound?.onNearVenuesChanged()
        }

        override fun onError(e: Throwable) {
            preventCallingApi()
            findNearVenues()
            requiredVenueOutputBound()
            venueOutputBound?.onNearVenuesChanged()
        }

        override fun onNoMoreVenues() {
            preventCallingApi()
        }
    }

    private fun preventCallingApi() {
        stopCallingApi = true
    }

    @Synchronized
    private fun findNearVenues() {

        nearVenues.clear()

        val allVenues = venuesStorageGateway.all()

        allVenues.forEach { v ->
            addVenue(v)
        }
    }

    fun addVenue(venue: Venue) {
        if (Radar.isNear(venue))
            nearVenues.add(venue)
    }

    lateinit var venueCache: VenueCacheOnUserMoved

    fun cacheVenuesWhenLocationChanged(location: Location) {

        venueCache = VenueCacheOnUserMoved(
            venuesApiGateway,
            venuesStorageGateway,
            location,
            cacheCompleted
        )
        venueCache.cacheFirst()
    }

    fun letApiCalled() {
        stopCallingApi = false
    }

    private fun cacheRestOfVenuesAtSameLocation() {
        if (canCallApi())
            venueCache.cacheRest()
    }

    private fun canCallApi() = !stopCallingApi

    @Synchronized
    fun getVenues(limit: Int, offset: Int): MutableList<Venue> {
        val allNear = ArrayList<Venue>(nearVenues)
        val vens = allNear.subList(
            allNear.size.coerceAtMost(offset),
            allNear.size.coerceAtMost(offset + limit)
        )

        if (vens.size < limit) {
            cacheRestOfVenuesAtSameLocation()
        }

        return vens
    }

    fun setVenueOutputBound(outputBound: VenueOutputBound) {
        this.venueOutputBound = outputBound
    }

    override fun loadVenues(limit: Int, offset: Int): MutableList<VenueInput> {
        requiredVenueOutputBound()

        val chunk = ArrayList<Venue>()
        chunk.addAll(getVenues(limit, offset))

        return MutableList(chunk.size) { index ->
            VenueInput()
                .apply {
                identifier = chunk[index].identifier
                name = chunk[index].name
                address = chunk[index].address
                lat = chunk[index].location.lat
                lon = chunk[index].location.lon
            }
        }
    }

    private fun requiredVenueOutputBound() {
        if (venueOutputBound == null)
            throw IllegalStateException("VenueOutputBound is null")
    }

    fun getCount(): Int {
        return nearVenues.size
    }
}