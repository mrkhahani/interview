package com.example.interview.venuesNearUser.apiImpl.venueDetail.model.getVenueDetail.response

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class VenueDetailResponse {
    @SerializedName("meta")
    @Expose
    var meta: Meta? =
        null
    @SerializedName("response")
    @Expose
    var response: Response? =
        null

}