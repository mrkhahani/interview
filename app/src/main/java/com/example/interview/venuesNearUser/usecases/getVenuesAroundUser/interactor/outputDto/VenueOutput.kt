package com.example.interview.venuesNearUser.usecases.getVenuesAroundUser.interactor.outputDto

class VenueOutput {
    var identifier: String = ""
    var name: String = ""
    var lat: Double = 0.0
    var lon: Double = 0.0
    var address: String = ""
}
