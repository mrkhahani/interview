package com.example.interview.venuesNearUser.android.ui.showVenueDetails

import android.app.Application
import androidx.lifecycle.ViewModel
import com.example.interview.venuesNearUser.android.TheApp
import com.example.interview.venuesNearUser.usecases.showVenueDetails.interactor.inputDto.VenueDetailInput

class VenueDetailViewModel(application: Application) : ViewModel() {

    private var app = application as TheApp
    var venueDetail: VenueDetailInput = VenueDetailInput()

}
