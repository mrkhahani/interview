package com.example.interview.venuesNearUser.android.ui._base

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.example.interview.venuesNearUser.android.TheApp

abstract class BaseFragment : Fragment() {

    protected lateinit var app: TheApp

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        app = requireContext().applicationContext as TheApp
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        initialViewModel()
        initialBinding(inflater, container)
        letBindingKnowViewModel()
        onCreateViewYourCode(savedInstanceState)
        return getRoot()
    }

    abstract fun onCreateViewYourCode(savedInstanceState: Bundle?)

    protected abstract fun getRoot(): View
    protected abstract fun initialViewModel()
    protected abstract fun initialBinding(
        inflater: LayoutInflater,
        container: ViewGroup?
    )

    protected abstract fun letBindingKnowViewModel()

}