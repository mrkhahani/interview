package com.example.interview.venuesNearUser.entities.radar

import com.example.interview.venuesNearUser.entities.venue.Venue

object Radar {

    var RADIUS: Int = 2000

    var centerPoint: Location? = null

    fun isNear(venue: Venue): Boolean {

        if (centerPoint == null)
            throw IllegalStateException("Location must specify")

        return Distance.estimate(centerPoint!!, venue.location) <= RADIUS
    }

}