package com.example.interview.venuesNearUser.apiImpl.venueDetail.model.getVenueDetail.response

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class Group____ {
    @SerializedName("type")
    @Expose
    var type: String? = null
    @SerializedName("name")
    @Expose
    var name: String? = null
    @SerializedName("count")
    @Expose
    var count: Int? = null
    @SerializedName("items")
    @Expose
    var items: List<Item___>? = null

}