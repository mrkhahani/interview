package com.example.interview.venuesNearUser.databaseImpl.venue

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.interview.common.storage.database.model.Venue

@Dao
interface VenueDoa {
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insert(venue: Venue): Long

    @Query("SELECT * FROM venue")
    fun all(): List<Venue>
}