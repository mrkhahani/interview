package com.example.interview.venuesNearUser.usecases.showVenueDetails.boundary.gateway.cache

import com.example.interview.common.monitor.Logger
import com.example.interview.venuesNearUser.entities.venueDetail.VenueDetail
import io.reactivex.rxjava3.core.Completable
import io.reactivex.rxjava3.core.CompletableObserver
import io.reactivex.rxjava3.core.Observer
import io.reactivex.rxjava3.disposables.Disposable
import io.reactivex.rxjava3.schedulers.Schedulers

abstract class VenueDetailCache(val callback: CacheCompleteCallback) {

    protected lateinit var venuesDetail: VenueDetail

    fun cache() {
        fetch()
    }

    protected abstract fun fetch()
    protected abstract fun store()

    protected fun whenVenuesReceived(): Observer<VenueDetail> {
        return object : Observer<VenueDetail> {
            override fun onComplete() {
            }

            override fun onSubscribe(d: Disposable?) {
            }

            override fun onNext(venueDetailParam: VenueDetail) {
                Logger.d("KH", "${venueDetailParam.identifier} venueDetail received from api")

                this@VenueDetailCache.venuesDetail = venueDetailParam
                storeAsync()
            }

            override fun onError(e: Throwable) {
                callback.onError(e)
            }
        }
    }

    private fun storeAsync() {
        Completable.fromCallable {
            store()
        }.subscribeOn(Schedulers.io())
            .subscribe(object : CompletableObserver {
                override fun onComplete() {
                    callback.onCached()
                }

                override fun onSubscribe(d: Disposable?) {
                }

                override fun onError(e: Throwable) {
                    callback.onError(e)
                }
            })
    }
}