package com.example.interview.venuesNearUser.usecases.detectUserMoves.interactor.inputDto

class LocationInput(
    var lat: Double,
    var lon: Double
)