package com.example.interview.venuesNearUser.android

import android.app.Application
import com.example.interview.common.storage.database.TheDb
import com.example.interview.venuesNearUser.android.controller.MovementController
import com.example.interview.venuesNearUser.android.controller.VenueController
import com.example.interview.venuesNearUser.android.controller.VenueDetailController
import com.example.interview.venuesNearUser.apiImpl.venue.VenuesApiGatewayImpl
import com.example.interview.venuesNearUser.apiImpl.venueDetail.VenueDetailApiGatewayImpl
import com.example.interview.venuesNearUser.databaseImpl.venue.VenueStorageGatewayImpl
import com.example.interview.venuesNearUser.databaseImpl.venueDetail.VenueDetailStorageGatewayImpl
import com.example.interview.venuesNearUser.fileImpl.user.UserStorageGatewayImpl
import com.example.interview.venuesNearUser.usecases.TransactionsMediator
import com.example.interview.venuesNearUser.usecases.detectUserMoves.interactor.MovementTransaction
import com.example.interview.venuesNearUser.usecases.getVenuesAroundUser.interactor.VenueTransaction
import com.example.interview.venuesNearUser.usecases.showVenueDetails.interactor.VenueDetailTransaction
import com.facebook.stetho.Stetho

class TheApp : Application() {

    lateinit var venueDetailTransaction: VenueDetailTransaction
    lateinit var movementTransaction: MovementTransaction
    lateinit var movementController: MovementController
    lateinit var venueTransaction: VenueTransaction
    lateinit var transactionsMediator: TransactionsMediator
    lateinit var venueController: VenueController
    lateinit var venueDetailController: VenueDetailController

    private fun main() {
        movementTransaction =
            MovementTransaction(
                UserStorageGatewayImpl(
                    this@TheApp
                )
            )
        movementController = MovementController(movementTransaction)

        venueTransaction =
            VenueTransaction(
                VenueStorageGatewayImpl(
                    TheDb.getInstance(this@TheApp)
                ),
                VenuesApiGatewayImpl()
            )
        venueController = VenueController(venueTransaction)

        transactionsMediator =
            TransactionsMediator(
                movementTransaction,
                venueTransaction
            )

        venueDetailTransaction = VenueDetailTransaction(
            VenueDetailApiGatewayImpl(),
            VenueDetailStorageGatewayImpl(TheDb.getInstance(this@TheApp))
        )

        venueDetailController = VenueDetailController(venueDetailTransaction)

    }

    override fun onCreate() {
        Stetho.initializeWithDefaults(this)
        super.onCreate()
        main()
    }


}