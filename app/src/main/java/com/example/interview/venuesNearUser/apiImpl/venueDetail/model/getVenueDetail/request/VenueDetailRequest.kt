package com.example.interview.venuesNearUser.apiImpl.venueDetail.model.getVenueDetail.request

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class VenueDetailRequest {

    @Expose
    @SerializedName("venueId")
    var venueId: String = ""

    fun fetchMap(): MutableMap<String, String> {
        return HashMap<String, String>().apply {
            this["venueId"] = venueId
        }
    }
}