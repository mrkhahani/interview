package com.example.interview.venuesNearUser.databaseImpl.venue

import com.example.interview.common.storage.database.TheDb
import com.example.interview.venuesNearUser.databaseGateway.venue.VenuesStorageGateway
import com.example.interview.venuesNearUser.entities.radar.Location
import com.example.interview.venuesNearUser.entities.venue.Venue

class VenueStorageGatewayImpl(private val db: TheDb) :
    VenuesStorageGateway {
    private val DEFAULT_ID = 0

    override fun save(venue: Venue): Long {
        val venueDbModel = com.example.interview.common.storage.database.model.Venue(
            DEFAULT_ID,
            venue.identifier,
            venue.location.lat,
            venue.location.lon,
            venue.name,
            venue.address
        )
        return db.venueDoa().insert(venueDbModel)
    }

    override fun all(): ArrayList<Venue> {
        val all = db.venueDoa().all()
        val result = ArrayList<Venue>()
        for (v in all)
            result.add(Venue().apply {
                this.name = v.name
                this.address = v.address
                this.identifier = v.serverId
                this.location =
                    Location(
                        v.lat,
                        v.lon
                    )
            })
        return result
    }
}