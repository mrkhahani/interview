package com.example.interview.venuesNearUser.usecases

import com.example.interview.venuesNearUser.entities.radar.Location
import com.example.interview.venuesNearUser.usecases.detectUserMoves.boundary.outputs.MovementOutputBound
import com.example.interview.venuesNearUser.usecases.detectUserMoves.interactor.MovementTransaction
import com.example.interview.venuesNearUser.usecases.detectUserMoves.interactor.outputDto.LocationOutput
import com.example.interview.venuesNearUser.usecases.getVenuesAroundUser.interactor.VenueTransaction

class TransactionsMediator(
    movementTransaction: MovementTransaction,
    private val venueTransaction: VenueTransaction
) {

    init {
        movementTransaction.addMovementOutputBound(object : MovementOutputBound {
            override fun onMoved(location: LocationOutput) {
                venueTransaction.letApiCalled()
                venueTransaction.cacheVenuesWhenLocationChanged(
                    Location(
                        location.lat,
                        location.lon
                    )
                )
            }
        })
    }

}