package com.example.interview.venuesNearUser.android.services

import android.app.IntentService
import android.content.Intent
import android.content.IntentSender
import android.os.Looper
import com.example.interview.venuesNearUser.android.TheApp
import com.example.interview.venuesNearUser.usecases.detectUserMoves.interactor.inputDto.LocationInput
import com.google.android.gms.common.api.ResolvableApiException
import com.google.android.gms.location.*
import com.google.android.gms.tasks.Task

class LocationService : IntentService("location_service") {

    private var fusedLocationClient: FusedLocationProviderClient? = null
    private lateinit var locationCallback: LocationCallback
    private var requestingLocationUpdates = true
    private lateinit var locationRequest: LocationRequest

    private lateinit var app: TheApp

    override fun onHandleIntent(intent: Intent?) {

        when (getAction(intent)) {
            "start" ->
                startService()
            "stop" ->
                stopService()
            else ->
                throw LocationServiceException("Location service called with wrong action.")
        }
    }

    private fun getAction(intent: Intent?): String {
        if (intent != null)
            if (intent.hasExtra("action"))
                return intent.getStringExtra("action")!!
        return "wrong action"
    }

    private fun stopService() {
        stopLocationUpdates()
    }

    private fun startService() {
        app = application as TheApp
        setupFusedLocationClient()
        setupLocationCallback()
        createLocationRequest()

        if (requestingLocationUpdates) startLocationUpdates()
    }

    private fun setupFusedLocationClient() {
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this)

        fusedLocationClient!!.lastLocation.addOnSuccessListener { location ->

            if (location == null)
                return@addOnSuccessListener

            val l =
                LocationInput(
                    location.latitude,
                    location.longitude
                )
            app.movementTransaction.onLocationChanged(l)

        }
        fusedLocationClient!!.lastLocation.addOnCompleteListener {

        }

    }

    private fun setupLocationCallback() {
        locationCallback = object : LocationCallback() {
            override fun onLocationResult(locationResult: LocationResult?) {
                locationResult ?: return
                for (location in locationResult.locations) {
                    app.movementController.locationChanged(location)
                }
            }
        }
    }

    private fun startLocationUpdates() {
        fusedLocationClient?.requestLocationUpdates(
            locationRequest,
            locationCallback,
            Looper.getMainLooper()
        )
    }

    private fun createLocationRequest() {
        locationRequest = LocationRequest.create()?.apply {
            this.interval = 10000
            this.fastestInterval = 5000
            priority =
                LocationRequest.PRIORITY_HIGH_ACCURACY //LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY
        }
            ?: return

        val builder = LocationSettingsRequest.Builder()
            .addLocationRequest(locationRequest)

        val client: SettingsClient = LocationServices.getSettingsClient(this)
        val task: Task<LocationSettingsResponse> = client.checkLocationSettings(builder.build())
        task.addOnSuccessListener {
            // All location settings are satisfied. The client can initialize
            // location requests here.
        }
        task.addOnFailureListener { exception ->
            if (exception is ResolvableApiException) {
                // Location settings are not satisfied, but this can be fixed
                // by showing the user a dialog.
                try {
                    // Show the dialog by calling startResolutionForResult(),
                    // and check the result in onActivityResult().

                    //todo use this feature later
//                    exception.startResolutionForResult(
//                        this,
//                        REQUEST_CHECK_SETTINGS
//                    )
                } catch (sendEx: IntentSender.SendIntentException) {
                    // Ignore the error.
                }
            } else {
                requestingLocationUpdates = false
            }
        }
    }

    private fun stopLocationUpdates() {
        if (fusedLocationClient == null) {
            fusedLocationClient = LocationServices.getFusedLocationProviderClient(this)
        }
        fusedLocationClient = null
    }

    class LocationServiceException(message: String) : Throwable(message)
}


