package com.example.interview.venuesNearUser.apiImpl.venueDetail.model.getVenueDetail.response

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class Colors {
    @SerializedName("highlightColor")
    @Expose
    var highlightColor: HighlightColor? = null
    @SerializedName("highlightTextColor")
    @Expose
    var highlightTextColor: HighlightTextColor? = null
    @SerializedName("algoVersion")
    @Expose
    var algoVersion: Int? = null

}