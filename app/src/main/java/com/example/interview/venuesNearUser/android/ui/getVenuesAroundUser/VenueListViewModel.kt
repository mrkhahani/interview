package com.example.interview.venuesNearUser.android.ui.getVenuesAroundUser

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import androidx.paging.PagedList
import com.example.interview.venuesNearUser.android.TheApp
import com.example.interview.venuesNearUser.usecases.getVenuesAroundUser.interactor.inputDto.VenueInput


class VenueListViewModel(application: Application) : AndroidViewModel(application) {

    val app: TheApp = application as TheApp

    var nearVenusLive: MutableLiveData<PagedList<VenueInput>> = MutableLiveData()

    init {

    }



}