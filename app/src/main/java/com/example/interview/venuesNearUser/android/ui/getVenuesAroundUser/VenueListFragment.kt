package com.example.interview.venuesNearUser.android.ui.getVenuesAroundUser

import android.Manifest
import android.annotation.SuppressLint
import android.app.PendingIntent
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.annotation.UiThread
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.paging.PagedList
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.interview.R
import com.example.interview.databinding.VenuesNearUserUsecaseGetVenuesAroundUserLayoutFragmentVenueListBinding
import com.example.interview.venuesNearUser.android.services.LocationService
import com.example.interview.venuesNearUser.android.ui.ViewModelFactory
import com.example.interview.venuesNearUser.android.ui._base.BaseFragment
import com.example.interview.venuesNearUser.android.ui.getVenuesAroundUser.adapter.VenueAdapter
import com.example.interview.venuesNearUser.android.ui.getVenuesAroundUser.adapter.VenuesPagingDataSource
import com.example.interview.venuesNearUser.android.ui.getVenuesAroundUser.adapter.listener.VenueClickListener
import com.example.interview.venuesNearUser.usecases.detectUserMoves.boundary.outputs.MovementOutputBound
import com.example.interview.venuesNearUser.usecases.detectUserMoves.interactor.outputDto.LocationOutput
import com.example.interview.venuesNearUser.usecases.getVenuesAroundUser.boundary.outputs.VenueOutputBound
import com.example.interview.venuesNearUser.usecases.getVenuesAroundUser.interactor.inputDto.VenueInput
import com.google.android.gms.location.ActivityRecognition
import com.google.android.gms.location.ActivityTransition
import com.google.android.gms.location.ActivityTransitionRequest
import com.google.android.gms.location.DetectedActivity
import com.google.android.gms.tasks.Task
import java.util.concurrent.Executors


class VenueListFragment : BaseFragment(), MovementOutputBound,
    VenueOutputBound {

    private lateinit var viewModel: VenueListViewModel
    private lateinit var binding: VenuesNearUserUsecaseGetVenuesAroundUserLayoutFragmentVenueListBinding
    private lateinit var venueAdapter: VenueAdapter

    private val MY_PERMISSIONS_REQUEST_ACTIVITY_RECOGNITION = 2
    private val MY_PERMISSIONS_REQUEST_FINE_LOCATION = 1
    private val REQUEST_ACTIVITY_TRANSITION_UPDATE = 100

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        app.movementTransaction.addMovementOutputBound(this)
        app.venueTransaction.setVenueOutputBound(this)

        //setupActivityRecognitionPermission()
    }

    override fun onCreateViewYourCode(savedInstanceState: Bundle?) {

        venueAdapter = VenueAdapter(onVenueItemClicked)
        binding.venuesList.adapter = venueAdapter
        binding.venuesList.layoutManager =
            LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)

        viewModel.nearVenusLive.observe(viewLifecycleOwner, Observer { venues ->
            if (venues == null)
                return@Observer

            venueAdapter.submitList(venues)
        })
    }

    private val onVenueItemClicked: VenueClickListener = object : VenueClickListener {
        override fun onItemClicked(venue: VenueInput) {
            gotoVenueDetailFragment(venue.identifier)
        }
    }

    private fun gotoVenueDetailFragment(venueId: String) {
        val action =
            VenueListFragmentDirections.actionVenueListFragmentToVenueDetailFragment(venueId)
        findNavController().navigate(action)
    }

    override fun getRoot(): View {
        return binding.root
    }

    override fun initialViewModel() {
        viewModel = ViewModelProvider(
            this, ViewModelFactory(app)
        ).get(VenueListViewModel::class.java)
    }

    override fun initialBinding(
        inflater: LayoutInflater,
        container: ViewGroup?
    ) {
        binding = DataBindingUtil.inflate(
            inflater,
            R.layout.venues_near_user_usecase_get_venues_around_user_layout_fragment_venue_list,
            container,
            false
        )
    }

    override fun letBindingKnowViewModel() {
        binding.viewModel = viewModel
    }

    private var pagingSource: VenuesPagingDataSource? = null
    private lateinit var pagedVenue: PagedList<VenueInput>

    private fun refreshVenues() {
        val PAGE_SIZE = 10
        val myConfig = PagedList.Config.Builder()
            .setInitialLoadSizeHint(PAGE_SIZE)
            .setPageSize(PAGE_SIZE)
            .setEnablePlaceholders(false)
            .build()
        val executor = Executors.newFixedThreadPool(1)
        pagingSource = VenuesPagingDataSource(app.venueController)

        pagedVenue =
            PagedList.Builder<Int, VenueInput>(pagingSource!!, myConfig)
                .setInitialKey(0)
                .setFetchExecutor(executor)
                .setNotifyExecutor(ContextCompat.getMainExecutor(requireContext()))
                .build()

        viewModel.nearVenusLive.postValue(null)
        viewModel.nearVenusLive.postValue(pagedVenue)
    }

    private fun setupLocationPermission() {
        // Here, thisActivity is the current activity
        if (ContextCompat.checkSelfPermission(
                requireActivity(),
                Manifest.permission.ACCESS_FINE_LOCATION
            )
            != PackageManager.PERMISSION_GRANTED
        ) {

            // Permission is not granted
            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(
                    requireActivity(),
                    Manifest.permission.ACCESS_FINE_LOCATION
                )
            ) {
                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.
            } else {
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(
                    requireActivity(),
                    arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                    MY_PERMISSIONS_REQUEST_FINE_LOCATION
                )
            }
        } else {
            // Permission has already been granted
            startLocationService()
        }
    }

    private fun startLocationService() {
        Intent(requireContext(), LocationService::class.java).also { intent ->
            intent.putExtra("action", "start")
            requireActivity().startService(intent)
        }
    }

    private fun stopLocationService() {
        Intent(requireContext(), LocationService::class.java).also { intent ->
            intent.putExtra("action", "stop")
            requireActivity().startService(intent)
        }
    }

    private fun setupActivityRecognitionPermission() {
        // Here, thisActivity is the current activity
        if (ContextCompat.checkSelfPermission(
                requireActivity(),
                Manifest.permission.ACTIVITY_RECOGNITION
            )
            != PackageManager.PERMISSION_GRANTED
        ) {

            // Permission is not granted
            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(
                    requireActivity(),
                    Manifest.permission.ACTIVITY_RECOGNITION
                )
            ) {
                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.
            } else {
                // No explanation needed, we can request the permission.
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                    ActivityCompat.requestPermissions(
                        requireActivity(),
                        arrayOf(Manifest.permission.ACTIVITY_RECOGNITION),
                        MY_PERMISSIONS_REQUEST_ACTIVITY_RECOGNITION
                    )
                }
            }
        } else {
            // Permission has already been granted
            setupActivityRecognitionClient()
        }
    }

    @SuppressLint("MissingPermission")
    private fun setupActivityRecognitionClient() {
        val request: ActivityTransitionRequest = buildTransitionRequest()
        val task: Task<*> = ActivityRecognition.getClient(requireActivity())
            .requestActivityTransitionUpdates(request, getActivityRecognitionPendingIntent())
        task.addOnSuccessListener {
            //todo Handle success...
        }
        task.addOnFailureListener {
            //todo Handle failure...
        }
    }

    private fun getActivityRecognitionPendingIntent(): PendingIntent? {
        return PendingIntent.getService(
            context,
            REQUEST_ACTIVITY_TRANSITION_UPDATE,
            requireActivity().intent,
            PendingIntent.FLAG_UPDATE_CURRENT
        )
    }

    private fun buildTransitionRequest(): ActivityTransitionRequest {
        val transitions: MutableList<ActivityTransition> = ArrayList()
        transitions.add(
            ActivityTransition.Builder()
                .setActivityType(DetectedActivity.IN_VEHICLE)
                .setActivityTransition(ActivityTransition.ACTIVITY_TRANSITION_ENTER)
                .build()
        )
        transitions.add(
            ActivityTransition.Builder()
                .setActivityType(DetectedActivity.IN_VEHICLE)
                .setActivityTransition(ActivityTransition.ACTIVITY_TRANSITION_EXIT)
                .build()
        )
        transitions.add(
            ActivityTransition.Builder()
                .setActivityType(DetectedActivity.WALKING)
                .setActivityTransition(ActivityTransition.ACTIVITY_TRANSITION_ENTER)
                .build()
        )
        transitions.add(
            ActivityTransition.Builder()
                .setActivityType(DetectedActivity.WALKING)
                .setActivityTransition(ActivityTransition.ACTIVITY_TRANSITION_EXIT)
                .build()
        )

        //todo: there also RUNNING, ON_BICYCLE, STILL, ON_FOOT

        return ActivityTransitionRequest(transitions)
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>, grantResults: IntArray
    ) {
        when (requestCode) {
            MY_PERMISSIONS_REQUEST_FINE_LOCATION -> {
                // If request is cancelled, the result arrays are empty.
                if ((grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                    startLocationService()
                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return
            }

            MY_PERMISSIONS_REQUEST_ACTIVITY_RECOGNITION -> {
                // If request is cancelled, the result arrays are empty.
                if ((grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                    setupActivityRecognitionClient()
                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return
            }

            // Add other 'when' lines to check for other
            // permissions this app might request.
            else -> {
                // Ignore all other requests.
            }
        }
    }

    override fun onResume() {
        super.onResume()
        setupLocationPermission()
    }

    override fun onStop() {
        super.onStop()
        stopLocationService()
    }

    override fun onMoved(location: LocationOutput) {
        if (!isAdded)
            return

        Toast.makeText(
            requireContext(),
            "${location.lat}, ${location.lon}", Toast.LENGTH_SHORT
        )
            .show()

    }

    @UiThread
    override fun onNearVenuesChanged() {
        refreshVenues()
    }
}
