package com.example.interview.venuesNearUser.apiImpl.venueDetail.model.getVenueDetail.response

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class Popular {
    @SerializedName("isOpen")
    @Expose
    var isOpen: Boolean? = null
    @SerializedName("isLocalHoliday")
    @Expose
    var isLocalHoliday: Boolean? = null
    @SerializedName("timeframes")
    @Expose
    var timeframes: List<Timeframe>? = null

}