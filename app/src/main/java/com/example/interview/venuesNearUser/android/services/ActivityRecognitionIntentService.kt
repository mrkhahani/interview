package com.example.interview.venuesNearUser.android.services

import android.content.Context
import android.content.Intent
import androidx.core.app.JobIntentService
import com.google.android.gms.location.ActivityTransitionResult


class ActivityRecognitionIntentService : JobIntentService() {
    override fun onHandleWork(intent: Intent) {
        if (ActivityTransitionResult.hasResult(intent)) {
            val result = ActivityTransitionResult.extractResult(intent)
            for (event in result!!.transitionEvents) {
                //todo handle activity recognition
            }
        }
    }

    companion object {

        const val JOB_ID = 1000

        fun enqueueWork(context: Context, work: Intent) {
            enqueueWork(
                context,
                ActivityRecognitionIntentService::class.java,
                JOB_ID,
                work
            )
        }
    }
}