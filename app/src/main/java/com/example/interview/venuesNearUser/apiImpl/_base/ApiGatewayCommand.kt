package com.example.interview.venuesNearUser.apiImpl._base

import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.core.Observer
import io.reactivex.rxjava3.functions.Function
import io.reactivex.rxjava3.schedulers.Schedulers

abstract class ApiGatewayCommand<ResponseType, EntityType> {

    fun execute(observer: Observer<EntityType>) {
        this.observer = observer
        attachSubscriber(call())
    }

    protected abstract fun call(): Observable<ResponseType>

    private lateinit var observer: Observer<EntityType>

    private fun attachSubscriber(response: Observable<ResponseType>) {
        response
            .subscribeOn(Schedulers.io())
            .subscribeOn(AndroidSchedulers.mainThread())
            .map(transform)
            .subscribe(observer)
    }

    protected abstract fun prepareRequest(): MutableMap<String, String>

    protected abstract val transform: Function<ResponseType, EntityType>
}