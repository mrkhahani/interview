package com.example.interview.venuesNearUser.usecases.detectUserMoves.interactor

import com.example.interview.common.monitor.Logger
import com.example.interview.venuesNearUser.entities.radar.Location
import com.example.interview.venuesNearUser.entities.radar.Radar
import com.example.interview.venuesNearUser.entities.user.User
import com.example.interview.venuesNearUser.fileGateway.user.UserStorageGateway
import com.example.interview.venuesNearUser.usecases.detectUserMoves.boundary.inputs.MovementInputBound
import com.example.interview.venuesNearUser.usecases.detectUserMoves.boundary.outputs.MovementOutputBound
import com.example.interview.venuesNearUser.usecases.detectUserMoves.interactor.inputDto.LocationInput
import com.example.interview.venuesNearUser.usecases.detectUserMoves.interactor.outputDto.LocationOutput

class MovementTransaction(
    private var userStorageGateway: UserStorageGateway
) : MovementInputBound {

    private var movementOutputBounds = ArrayList<MovementOutputBound>()
    private var user = User()

    fun getCurrentLocation() = user.getLatestLocation()

    fun movedTo(location: Location) {
        user.moves(location)
        if (user.isMoved(location)) {
            Radar.centerPoint = user.getLatestLocation()
            userStorageGateway.saveUserLocation(location)
            val currentLocation = getCurrentLocation()

            movementOutputBounds.forEach {
                it.onMoved(LocationOutput().apply {
                    lat = currentLocation.lat
                    lon = currentLocation.lon
                })
            }

            Logger.d("KH", "Location: ${location.lat}, ${location.lon}")
        }
    }

    override fun onLocationChanged(location: LocationInput) {
        movedTo(
            Location(
                location.lat,
                location.lon
            )
        )
    }

    fun addMovementOutputBound(outputBound: MovementOutputBound) {
        this.movementOutputBounds.add(outputBound)
    }


}