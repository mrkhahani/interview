package com.example.interview.venuesNearUser.usecases.getVenuesAroundUser.boundary.inputs

import com.example.interview.venuesNearUser.usecases.getVenuesAroundUser.interactor.inputDto.VenueInput

interface VenueInputBound {
    fun loadVenues(limit: Int, offset: Int): MutableList<VenueInput>
}