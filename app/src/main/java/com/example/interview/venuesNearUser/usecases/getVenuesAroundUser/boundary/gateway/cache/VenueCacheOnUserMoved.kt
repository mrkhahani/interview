package com.example.interview.venuesNearUser.usecases.getVenuesAroundUser.boundary.gateway.cache

import com.example.interview.venuesNearUser.apiGateway.veneu.VenuesApiGateway
import com.example.interview.venuesNearUser.databaseGateway.venue.VenuesStorageGateway
import com.example.interview.venuesNearUser.entities.radar.Location

class VenueCacheOnUserMoved(
    private val venuesApiGateway: VenuesApiGateway,
    private val venuesStorageGateway: VenuesStorageGateway,
    private val location: Location,
    callback: CacheCompleteCallback
) : VenueCache(callback) {

    override fun fetchRest() {
        venuesApiGateway.fetchRestVenues(whenVenuesReceived())
    }

    override fun fetch() {
        venuesApiGateway.fetchFirstVenues(location, whenVenuesReceived())
    }

    override fun store() {
        venues.forEach { v ->
            venuesStorageGateway.save(v)
        }
    }
}