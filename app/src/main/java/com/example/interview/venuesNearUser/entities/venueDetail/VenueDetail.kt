package com.example.interview.venuesNearUser.entities.venueDetail

class VenueDetail : Cloneable {
    var identifier: String = ""
    var likes: Int = 0
    var rate: Double = 0.0
    var ratingSingal: Int = 0
    var phone: String = ""

    public override fun clone(): Any {
        return super.clone()
    }
}