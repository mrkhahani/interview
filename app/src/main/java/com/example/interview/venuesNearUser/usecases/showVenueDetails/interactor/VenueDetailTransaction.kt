package com.example.interview.venuesNearUser.usecases.showVenueDetails.interactor

import com.example.interview.venuesNearUser.apiGateway.venueDetail.VenueDetailApiGateway
import com.example.interview.venuesNearUser.databaseGateway.venueDetail.VenueDetailStorageGateway
import com.example.interview.venuesNearUser.entities.venue.Venue
import com.example.interview.venuesNearUser.entities.venueDetail.VenueDetail
import com.example.interview.venuesNearUser.usecases.showVenueDetails.boundary.gateway.cache.CacheCompleteCallback
import com.example.interview.venuesNearUser.usecases.showVenueDetails.boundary.gateway.cache.VenueDetailCacheImpl
import com.example.interview.venuesNearUser.usecases.showVenueDetails.boundary.input.VenueDetailInputBound
import com.example.interview.venuesNearUser.usecases.showVenueDetails.boundary.output.VenueDetailOutputBound
import com.example.interview.venuesNearUser.usecases.showVenueDetails.interactor.inputDto.VenueInput

class VenueDetailTransaction(
    val venueDetailApiGateway: VenueDetailApiGateway,
    val venueDetailStorageGateway: VenueDetailStorageGateway
) : VenueDetailInputBound {

    @Volatile
    private lateinit var venueDetail: VenueDetail

    private lateinit var venueDetailCache: VenueDetailCacheImpl

    private var venueDetailOutputBound: VenueDetailOutputBound? = null

    private val cacheCompleted = object : CacheCompleteCallback {
        override fun onCached() {
            fillVenueDetail()
        }

        override fun onError(e: Throwable) {
            fillVenueDetail()
        }
    }

    private fun fillVenueDetail() {
        try {
            requiredVenueDetailOutputBound()
            venueDetail = venueDetailStorageGateway.get(venueDetail.identifier)

            venueDetailOutputBound?.onVenueDetailArrived(venueDetail)
        } catch (e: Exception) {
            venueDetailOutputBound?.onVenueDetailArrived(venueDetail)
        }
    }

    fun requiredVenueDetailOutputBound() {
        if (venueDetailOutputBound == null)
            throw IllegalStateException("VenueDetailOutputBound is require.")
    }


    @Synchronized
    override fun getVenueDetail(venue: VenueInput) {

        venueDetail = VenueDetail().apply {
            this.identifier = venue.identifier
        }

        venueDetailCache = VenueDetailCacheImpl(
            venueDetailApiGateway,
            venueDetailStorageGateway,
            Venue().apply {
                this.identifier = venue.identifier
            },
            cacheCompleted
        )
        venueDetailCache.cache()
    }

    fun setVenueDetailOutputBound(outputBound: VenueDetailOutputBound) {
        this.venueDetailOutputBound = outputBound
    }

}