package com.example.interview.venuesNearUser.apiImpl.venue.model.getVenues.response

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class Response {
    @SerializedName("suggestedFilters")
    @Expose
    var suggestedFilters: SuggestedFilters? = null
    @SerializedName("headerLocation")
    @Expose
    var headerLocation: String? = null
    @SerializedName("headerFullLocation")
    @Expose
    var headerFullLocation: String? = null
    @SerializedName("headerLocationGranularity")
    @Expose
    var headerLocationGranularity: String? = null
    @SerializedName("totalResults")
    @Expose
    var totalResults: Int? = null
    @SerializedName("suggestedBounds")
    @Expose
    var suggestedBounds: SuggestedBounds? = null
    @SerializedName("groups")
    @Expose
    var groups: List<Group>? =
        null

}