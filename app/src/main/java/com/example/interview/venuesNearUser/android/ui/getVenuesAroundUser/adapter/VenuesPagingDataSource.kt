package com.example.interview.venuesNearUser.android.ui.getVenuesAroundUser.adapter

import androidx.paging.PositionalDataSource
import com.example.interview.venuesNearUser.android.controller.VenueController
import com.example.interview.venuesNearUser.usecases.getVenuesAroundUser.interactor.inputDto.VenueInput

class VenuesPagingDataSource(private val venueController: VenueController) :
    PositionalDataSource<VenueInput>() {
    override fun loadRange(params: LoadRangeParams, callback: LoadRangeCallback<VenueInput>) {
        if (params.startPosition == 0)
            return
        val venues = venueController.loadVeneus(params.loadSize, params.startPosition)
        callback.onResult(venues)
    }

    override fun loadInitial(params: LoadInitialParams, callback: LoadInitialCallback<VenueInput>) {
        val venus =
            venueController.loadVeneus(params.pageSize, params.requestedStartPosition)
        callback.onResult(venus, params.requestedStartPosition + venus.size)
    }


}