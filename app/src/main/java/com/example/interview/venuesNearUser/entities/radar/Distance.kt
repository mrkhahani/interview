package com.example.interview.venuesNearUser.entities.radar

import kotlin.math.*

class Distance {
    companion object {
        /**
         * @return distance in meter
         */
        @JvmStatic
        fun estimate(first: Location, second: Location): Double {
            // The math module contains a function
            // named toRadians which converts from
            // degrees to radians.
            var lat1 = first.lat
            var lat2 = second.lat
            var lon1 = first.lon
            var lon2 = second.lon
            lon1 = Math.toRadians(lon1)
            lon2 = Math.toRadians(lon2)
            lat1 = Math.toRadians(lat1)
            lat2 = Math.toRadians(lat2)

            // Haversine formula
            val dlon = lon2 - lon1
            val dlat = lat2 - lat1
            val a = (sin(dlat / 2).pow(2.0)
                    + (cos(lat1) * cos(lat2)
                    * sin(dlon / 2).pow(2.0)))
            val c = 2 * asin(sqrt(a))

            // Radius of earth in kilometers
            val r = 6371.0

            // convert kilometer to meter
            val m = 1000

            // calculate the result
            return ceil(c * r * m)
        }
    }
}