package com.example.interview.venuesNearUser.fileGateway.user

import com.example.interview.venuesNearUser.entities.radar.Location

interface UserStorageGateway {
    fun saveUserLocation(location: Location)
    fun getUserLocation(): Location
}