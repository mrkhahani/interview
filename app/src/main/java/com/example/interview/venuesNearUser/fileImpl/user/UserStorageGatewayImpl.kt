package com.example.interview.venuesNearUser.fileImpl.user

import android.content.Context
import com.example.core.sharedpreferences.SharedPrefSimple
import com.example.interview.venuesNearUser.entities.radar.Location
import com.example.interview.venuesNearUser.fileGateway.user.UserStorageGateway

class UserStorageGatewayImpl(context: Context) :
    UserStorageGateway {

    private val USER_LOCATION_KEY = "user_location"
    private val shP = SharedPrefSimple(context)

    override fun saveUserLocation(location: Location) {
        shP.write(USER_LOCATION_KEY, locationToString(location))
    }

    override fun getUserLocation(): Location {
        return stringToLocation(shP.read(USER_LOCATION_KEY))
    }

    private fun locationToString(location: Location): String {
        return "${location.lat}-${location.lon}"
    }

    private fun stringToLocation(str: String): Location {
        val location =
            Location(
                0.0,
                0.0
            )

        try{
            location.lat = str.split("-")[0].toDouble()
            location.lon = str.split("-")[1].toDouble()
        }finally {
            return location
        }
    }
}