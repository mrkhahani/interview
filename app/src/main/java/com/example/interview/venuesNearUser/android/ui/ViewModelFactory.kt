package com.example.interview.venuesNearUser.android.ui

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.interview.venuesNearUser.android.TheApp
import com.example.interview.venuesNearUser.android.ui.getVenuesAroundUser.VenueListViewModel
import com.example.interview.venuesNearUser.android.ui.showVenueDetails.VenueDetailViewModel

@Suppress("UNCHECKED_CAST")
class ViewModelFactory(private val application: TheApp) : ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(VenueListViewModel::class.java)) {
            return VenueListViewModel(
                application
            ) as T
        }
        if (modelClass.isAssignableFrom(VenueDetailViewModel::class.java)) {
            return VenueDetailViewModel(
                application
            ) as T
        }

        throw IllegalArgumentException("Unknown view model")
    }
}