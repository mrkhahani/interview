package com.example.interview.venuesNearUser.usecases.getVenuesAroundUser.boundary.gateway.cache

import com.example.interview.common.monitor.Logger
import com.example.interview.venuesNearUser.entities.venue.Venue
import io.reactivex.rxjava3.core.Completable
import io.reactivex.rxjava3.core.CompletableObserver
import io.reactivex.rxjava3.core.Observer
import io.reactivex.rxjava3.disposables.Disposable
import io.reactivex.rxjava3.schedulers.Schedulers

abstract class VenueCache(val callback: CacheCompleteCallback) {

    protected lateinit var venues: ArrayList<Venue>

    fun cacheFirst() {
        fetch()
    }

    fun cacheRest() {
        fetchRest()
    }

    protected abstract fun fetchRest()
    protected abstract fun fetch()
    protected abstract fun store()

    protected fun whenVenuesReceived(): Observer<ArrayList<Venue>> {
        return object : Observer<ArrayList<Venue>> {
            override fun onComplete() {
            }

            override fun onSubscribe(d: Disposable?) {
            }

            override fun onNext(venues: ArrayList<Venue>) {
                Logger.d("KH", "${venues.size} venues received from api")

                if (venues.size <= 0) {
                    callback.onNoMoreVenues()
                }

                this@VenueCache.venues = venues
                storeAsync()
            }

            override fun onError(e: Throwable) {
                callback.onError(e)
            }
        }
    }

    private fun storeAsync() {
        Completable.fromCallable {
            store()
        }.subscribeOn(Schedulers.io())
            .subscribe(object : CompletableObserver {
                override fun onComplete() {
                    callback.onCached()
                }

                override fun onSubscribe(d: Disposable?) {
                }

                override fun onError(e: Throwable) {
                    callback.onError(e)
                }
            })
    }
}