package com.example.interview.venuesNearUser.entities

import com.example.interview.venuesNearUser.entities.radar.Distance
import com.example.interview.venuesNearUser.entities.radar.Location
import com.example.interview.venuesNearUser.entities.user.User
import com.example.interview.venuesNearUser.entities.venue.Venue
import org.junit.Assert.assertEquals
import org.junit.Test

internal class DistanceTest{

    private fun assertDistance(
        venueLocation: Location,
        userLocation: Location
    ): Double {
        val venue = Venue()
        venue.location = venueLocation

        val user = User()
        user.moves(userLocation)

        return Distance.estimate(user.getLatestLocation(), venue.location)
    }

    @Test
    fun given_distance_same_location_then_distance_is_zero() {

        val location =
            Location(
                35.68712830304926,
                51.40478497364437
            )

        val distance = assertDistance(location, location)

        assertEquals(distance, 0.0, 0.0)
    }

    @Test
    fun given_distance_difference_location_given_distance() {

        val venueLocation =
            Location(
                37.423372,
                -122.095936
            )
        val userLocation =
            Location(
                37.424243,
                -122.097091
            )

        val distance = assertDistance(venueLocation, userLocation)

        assertEquals(distance, 141.0,0.0)
    }

    @Test
    fun given_distance_difference_location_given_distance2() {

        val venueLocation =
            Location(
                35.68712830304926,
                51.40478497364437
            )
        val userLocation =
            Location(
                35.700406,
                51.388156
            )

        val distance = assertDistance(venueLocation, userLocation)

        assertEquals(distance, 2106.0,0.0)
    }
}