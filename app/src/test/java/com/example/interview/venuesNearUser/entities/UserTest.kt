package com.example.interview.venuesNearUser.entities

import com.example.interview.venuesNearUser.entities.radar.Location
import com.example.interview.venuesNearUser.entities.user.User
import org.junit.Assert.assertEquals
import org.junit.Test

internal class UserTest{
    @Test
    fun is_user_single_all_the_time(){
        val firstLocation =
            Location(
                35.700689,
                51.385873
            )
        val secondLocation =
            Location(
                35.700000,
                51.385000
            )
        val user = User()
        val stillTheUser =
            User()
        user.moves(firstLocation)
        stillTheUser.moves(secondLocation)

        assertEquals(secondLocation.lat, user.getLatestLocation().lat,0.0)
        assertEquals(secondLocation.lon, user.getLatestLocation().lon,0.0)
        assertEquals(secondLocation.lat, stillTheUser.getLatestLocation().lat,0.0)
        assertEquals(secondLocation.lon, stillTheUser.getLatestLocation().lon,0.0)

    }

    @Test
    fun user_moves(){
        val user = User()
        val location =
            Location(
                35.700689,
                51.385873
            )
        user.moves(location)
        assertEquals(location.lat, user.getLatestLocation().lat,0.0)
        assertEquals(location.lon, user.getLatestLocation().lon,0.0)
    }

    @Test
    fun user_move_less_than_100_rejected(){
        val user = User()
        val old = Location(
            37.424539,
            -122.097171
        )
        user.moves(old)

        val new = Location(
            37.424463,
            -122.097085
        )
        user.moves(new)

        assertEquals(old.lat, user.getLatestLocation().lat,0.0)
        assertEquals(old.lon, user.getLatestLocation().lon,0.0)
    }

    @Test
    fun when_user_moves_then_is_moved_return_true(){
        val user = User()
        val old = Location(
            37.424539,
            -122.097171
        )
        user.moves(old)

        val new = Location(
            37.534463,
            -122.097085
        )
        user.moves(new)

        assertEquals(user.isMoved(new), true)
    }

    @Test
    fun when_user_moves_to_less_than_100_distance_then_is_moved_return_false(){
        val user = User()
        val old = Location(
            37.424539,
            -122.097171
        )
        user.moves(old)

        val new = Location(
            37.424455,
            -122.097085
        )
        user.moves(new)

        assertEquals(user.isMoved(new), false)
    }
}