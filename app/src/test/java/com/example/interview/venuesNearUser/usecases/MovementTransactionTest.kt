package com.example.interview.venuesNearUser.usecases

import com.example.interview.venuesNearUser.entities.radar.Location
import com.example.interview.venuesNearUser.entities.venue.Venue
import com.example.interview.venuesNearUser.usecases.detectUserMoves.interactor.MovementTransaction
import com.example.interview.venuesNearUser.usecases.getVenuesAroundUser.boundary.gateway.cache.CacheCompleteCallback
import com.example.interview.venuesNearUser.usecases.getVenuesAroundUser.boundary.gateway.cache.VenueCacheOnUserMoved
import com.example.interview.venuesNearUser.usecases.getVenuesAroundUser.interactor.VenueTransaction
import com.example.interview.venuesNearUser.usecases.mock.MockApi
import com.example.interview.venuesNearUser.usecases.mock.MockStorage
import com.example.interview.venuesNearUser.usecases.mock.MockUserStorage
import org.junit.Assert
import org.junit.Test


internal class MovementTransactionTest {

    val veniues: ArrayList<Venue> = getAllVenues()

    private fun getAllVenues(): ArrayList<Venue> {
        val venues = ArrayList<Venue>()

        venues.add(Venue().apply {
            location =
                Location(
                    35.68712830304926,
                    51.40478497364437
                )
            name = "Moghadam Museum l موزه خانه مقدم (موزه خانه مقدم)"
            address = "No. 249, Imam Khomeini St."
        })

        venues.add(Venue().apply {
            location =
                Location(
                    35.700406,
                    51.388156
                )
            name = "Nikoo Sefat Aash | آش نیکو صفت (آش نیکو صفت)"
            address = "South Jamalzadeh St."
        })

        return venues
    }

    @Test
    fun given_venues_then_near_venues_collected() {

        val movement =
            MovementTransaction(
                MockUserStorage()
            )

        movement.movedTo(
            Location(
                35.700406,
                51.388156
            )
        )

        val venueTransaction =
            VenueTransaction(
                MockStorage(),
                MockApi()
            )

        for (v in veniues)
            venueTransaction.addVenue(v)

        Assert.assertEquals(venueTransaction.getCount(), 1)

    }

    @Test
    fun when_user_move_then_near_venues_check_again() {

        val movement =
            MovementTransaction(
                MockUserStorage()
            )
        movement.movedTo(
            Location(
                35.700406,
                51.388156
            )
        )

        val venueTransaction =
            VenueTransaction(
                MockStorage(),
                MockApi()
            )

        for (v in veniues)
            venueTransaction.addVenue(v)

        movement.movedTo(
            Location(
                35.68712830304926,
                51.40478497364437
            )
        )

        Assert.assertEquals(venueTransaction.getCount(), 1)
    }

    @Test
    fun when_request_near_venues_and_venues_is_empty_return_empty_list() {
        val movement =
            MovementTransaction(
                MockUserStorage()
            )
        val venueTransaction =
            VenueTransaction(
                MockStorage(),
                MockApi()
            )
        movement.movedTo(
            Location(
                35.700406,
                51.388156
            )
        )
        venueTransaction.venueCache = getFakeVenueCache(movement)
        Assert.assertEquals(venueTransaction.getVenues(10, 0).size, 0)
    }

    @Test
    fun when_request_near_venues_and_there_is_one_venues_then_return_one_venue() {

        val movement =
            MovementTransaction(
                MockUserStorage()
            )
        movement.movedTo(
            Location(
                35.700406,
                51.388156
            )
        )

        val venueTransaction =
            VenueTransaction(
                MockStorage(),
                MockApi()
            )
        for (v in veniues)
            venueTransaction.addVenue(v)

        venueTransaction.venueCache = getFakeVenueCache(movement)

        Assert.assertEquals(venueTransaction.getVenues(10, 0).size, 1)
    }

    private fun getFakeVenueCache(movement: MovementTransaction): VenueCacheOnUserMoved {
        return VenueCacheOnUserMoved(
            MockApi(),
            MockStorage(),
            movement.getCurrentLocation(),
            object : CacheCompleteCallback {
                override fun onCached() {
                }

                override fun onError(e: Throwable) {
                }

                override fun onNoMoreVenues() {
                }
            })
    }

    @Test
    fun when_request_venues_first_chunck_than_first_chunk_returns() {

        val movement =
            MovementTransaction(
                MockUserStorage()
            )
        movement.movedTo(
            Location(
                35.700406,
                51.388156
            )
        )

        val venTransactions =
            VenueTransaction(
                MockStorage(),
                MockApi()
            )
        for (v in addMoreVenues())
            venTransactions.addVenue(v)

        val nearVenues = venTransactions.getVenues(10, 0)
        Assert.assertEquals(nearVenues.size, 10)
        Assert.assertEquals(nearVenues[0].name, "0")
        Assert.assertEquals(nearVenues[1].name, "1")
        Assert.assertEquals(nearVenues[2].name, "2")
        Assert.assertEquals(nearVenues[3].name, "3")
        Assert.assertEquals(nearVenues[4].name, "4")
        Assert.assertEquals(nearVenues[9].name, "9")
    }

    @Test
    fun when_request_venues_second_chunk_then_second_chunk_returned() {

        val movement =
            MovementTransaction(
                MockUserStorage()
            )
        movement.movedTo(
            Location(
                35.700406,
                51.388156
            )
        )

        val venTransactions =
            VenueTransaction(
                MockStorage(),
                MockApi()
            )
        for (v in addMoreVenues())
            venTransactions.addVenue(v)

        val nearVenues = venTransactions.getVenues(10, 10)
        Assert.assertEquals(nearVenues.size, 10)
        Assert.assertEquals(nearVenues[0].name, "10")
        Assert.assertEquals(nearVenues[1].name, "11")
        Assert.assertEquals(nearVenues[2].name, "12")
        Assert.assertEquals(nearVenues[3].name, "13")
        Assert.assertEquals(nearVenues[4].name, "14")
        Assert.assertEquals(nearVenues[9].name, "19")
    }

    @Test
    fun when_request_venues_more_than_last_chunk_then_empty_list_return() {

        val movement =
            MovementTransaction(
                MockUserStorage()
            )
        movement.movedTo(
            Location(
                35.700406,
                51.388156
            )
        )

        val venueTransaction =
            VenueTransaction(
                MockStorage(),
                MockApi()
            )
        for (v in addMoreVenues())
            venueTransaction.addVenue(v)
        venueTransaction.venueCache = getFakeVenueCache(movement)
        val nearVenues = venueTransaction.getVenues(10, 100)
        Assert.assertEquals(nearVenues.size, 0)
    }

    @Test
    fun when_request_venues_last_chunk_then_last_chunk_return() {

        val movement =
            MovementTransaction(
                MockUserStorage()
            )
        movement.movedTo(
            Location(
                35.700406,
                51.388156
            )
        )

        val venueTransaction =
            VenueTransaction(
                MockStorage(),
                MockApi()
            )
        for (v in addMoreVenues())
            venueTransaction.addVenue(v)

        venueTransaction.venueCache = getFakeVenueCache(movement)
        val nearVenues = venueTransaction.getVenues(10, 95)
        Assert.assertEquals(nearVenues.size, 5)
    }

    private fun addMoreVenues(): ArrayList<Venue> {
        val result = ArrayList<Venue>()
        for (i in 0 until 100) {
            for (v in veniues) {
                val cVenue = v.clone() as Venue
                cVenue.name = i.toString()
                result.add(cVenue)
            }
        }
        return result
    }

}