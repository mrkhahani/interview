package com.example.interview.venuesNearUser.usecases.mock

import com.example.interview.venuesNearUser.databaseGateway.venue.VenuesStorageGateway
import com.example.interview.venuesNearUser.entities.venue.Venue

class MockStorage :
    VenuesStorageGateway {
    override fun save(venue: Venue): Long {
        return 0
    }

    override fun all(): ArrayList<Venue> {
        return ArrayList<Venue>()
    }
}