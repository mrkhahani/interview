package com.example.interview.venuesNearUser.usecases.mock

import com.example.interview.venuesNearUser.entities.radar.Location
import com.example.interview.venuesNearUser.fileGateway.user.UserStorageGateway

class MockUserStorage :
    UserStorageGateway {
    override fun saveUserLocation(location: Location) {

    }

    override fun getUserLocation(): Location {
        return Location(
            0.0,
            0.0
        )
    }
}