package com.example.interview.venuesNearUser.usecases.mock

import com.example.interview.venuesNearUser.apiGateway.veneu.VenuesApiGateway
import com.example.interview.venuesNearUser.entities.radar.Location
import com.example.interview.venuesNearUser.entities.venue.Venue
import io.reactivex.rxjava3.core.Observer

class MockApi :
    VenuesApiGateway {
    override fun fetchFirstVenues(location: Location, observerParam: Observer<ArrayList<Venue>>) {

    }

    override fun fetchRestVenues(observerParam: Observer<ArrayList<Venue>>) {

    }

}