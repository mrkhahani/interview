package com.example.interview.venuesNearUser.entities

import com.example.interview.venuesNearUser.entities.radar.Location
import com.example.interview.venuesNearUser.entities.radar.Radar
import com.example.interview.venuesNearUser.entities.user.User
import com.example.interview.venuesNearUser.entities.venue.Venue
import org.junit.Assert.assertEquals
import org.junit.Test

internal class RadarTest {

    private fun assertIsNear(
        radius: Int): Boolean {

        val venueLocation =
            Location(
                35.68712830304926,
                51.40478497364437
            )
        val userLocation =
            Location(
                35.700406,
                51.388156
            )
        // distance is 2160.0 meter

        val venue = Venue()
        venue.location = venueLocation
        val user = User()
        user.moves(userLocation)

        Radar.RADIUS = radius
        Radar.centerPoint = user.getLatestLocation()

        return Radar.isNear(venue)
    }

    @Test
    fun given_small_radius_for_long_distance_then_is_not_near() {
        val radius = 1000 // in meter
        assertEquals(assertIsNear(radius), false)
    }

    @Test
    fun given_large_radius_for_enough_distance_then_is_near() {
        val radius = 10000 // in meter
        assertEquals(assertIsNear(radius), true)
    }

    @Test
    fun given_exact_radius_for_exact_distance_then_is_near() {
        val radius = 2160 // in meter
        assertEquals(assertIsNear(radius), true)
    }

}